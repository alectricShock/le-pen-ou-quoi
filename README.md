
```sh
$ pip install -r requirements.txt

Create a Postgres DB le_pen_ou_quoi
Change setting user and pass word:
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.path.basename(PROJECT_DIR),
        'USER': 'your user',
        'PASSWORD': 'your Password',
        'HOST': os.environ.get('DB_HOST', '127.0.0.1'),
    }
}

$ python manage.py migrate

$ pg_restore --verbose --clean --no-acl --no-owner -h localhost -U abourdon -d le_pen_ou_quoi latest.dump


$ python manage.py collectstatic

$ python manage.py runserver
```
