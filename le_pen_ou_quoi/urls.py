from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

from le_pen.views import HomePageView, StandingsView, VoteView, ContenderView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^standings/$', StandingsView.as_view(), name='standings'),
    url(r'^contender/(?P<slug>.+)/$', ContenderView.as_view(), name='contender'),
    url(r'^vote/(?P<vote>.+)/(?P<contender>.+)/$', VoteView.as_view(), name='vote'),



]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
