from __future__ import absolute_import, unicode_literals
import os
import sys
from celery import Celery
from celery.schedules import crontab


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'le_pen_ou_quoi.settings')

from django.conf import settings
sys.path.append(os.path.join(settings.BASE_DIR, "apps"))

app = Celery('le_pen_ou_quoi')

# Using a string here means the worker don't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')


app.conf.update(BROKER_URL=settings.REDIS_URL,
                CELERY_RESULT_BACKEND=settings.REDIS_URL)

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


app.conf.beat_schedule = {
    'print_db': {
        'task': 'timesheet_api.tasks.print_db',
        'schedule': 10.0,

    },
}
