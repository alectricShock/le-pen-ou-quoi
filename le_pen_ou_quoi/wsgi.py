"""
WSGI config for le_pen_ou_quoi project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
import sys


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "le_pen_ou_quoi.settings")

from django.conf import settings
sys.path.append(os.path.join(settings.BASE_DIR, "apps"))

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

application = get_wsgi_application()
application = DjangoWhiteNoise(application)
