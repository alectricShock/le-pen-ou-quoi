# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.


class Photo(models.Model):
    slug = models.SlugField(max_length=60, unique=True)
    image = models.ImageField(upload_to='contender_image', blank=False, null=False)

    def __unicode__(self):
        return self.slug


class Contender(models.Model):
    slug = models.SlugField(max_length=60, unique=True)
    title = models.CharField(verbose_name='Title', max_length=255)
    votes = models.IntegerField(default=0)
    is_le_pen = models.BooleanField(default=False)
    images = models.ManyToManyField(Photo, blank=False)

    def __unicode__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('contender', kwargs={'slug': self.slug})

class Voter(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    session_key = models.CharField(max_length=100)
    contenders = models.ManyToManyField(Contender, blank=True)
    done_voting = models.BooleanField(default=False)
