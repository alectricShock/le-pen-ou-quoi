# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.staticfiles.storage import staticfiles_storage

import json
import requests

# from messengerbot import messenger



def convert_google_url(map_url):
    map_url = map_url.replace('\"','')
    url_list = map_url.split('?')
    web_url = 'https://www.google.com/maps?{}'.format(url_list[1])
    return web_url

def get_domain(request):
    return get_current_site(request).domain

def get_static_address(domain_url, file_name):

    static = staticfiles_storage.url(file_name)
    static_url = ''.join(['https://', domain_url, static])
    return static_url

def get_recipient_id(body_dict):
    try:
        return body_dict['originalRequest']['data']['sender']['id']
    except Exception as e:
        raise

def call_action(body_dict):
    action = body_dict['result']['action']
    try:
        return dispatcher[action](body_dict)
    except Exception as e:
        raise

def reply_store_location(body_dict, map_url, domain_url):
    recipient = messages.Recipient(recipient_id=get_recipient_id(body_dict))
    web_url = convert_google_url(map_url)
    # http://www.inserthtml.com/demo/accelerometer/index-2.php
    web_button = elements.WebUrlButton(
            title='Map',
            url=' https://timesheet.ngrok.io',
            webview_height_ratio='compact'
            )
    template = templates.ButtonTemplate(
            text='Here\'s the closest store with your product 🏬',
            buttons=[
            web_button
            ])
    attachment = attachments.TemplateAttachment(template=template)
    message = messages.Message(attachment=attachment)
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)
    message = messages.Message(text='thanks for trying le_pen Seph ☀️ 🏖️ 🕶️')
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)
    return

def reply_welcome(body_dict, fb_user_dict, recipient_id, domain_url):
    recipient = messages.Recipient(recipient_id=recipient_id)

    text = 'Hi {}! I\'m le_pen Seph ☀️ 🏖️ 🕶️'.format(fb_user_dict['first_name'])
    message = messages.Message(text=text)
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)
    # attachment = attachments.ImageAttachment(
    #         # url=get_static_address(domain_url, 'giphy.gif')
    #         url='https://media.giphy.com/media/qutisuYBaGfFC/giphy.gif'
    # )
    # message = messages.Message(attachment=attachment)
    # request = messages.MessageRequest(recipient, message)
    # messenger.send(request)

    quick_yes = quick_replies.QuickReplyItem(
            content_type='text',
            title='yes 👍🏼',
            payload='YES',
            )
    quick_no = quick_replies.QuickReplyItem(
            content_type='text',
            title='no 👎🏼',
            payload='NO',
            )
    quick_reps = quick_replies.QuickReplies(
            quick_replies= [quick_yes, quick_no],
            )
    message = messages.Message(text='I\'m here to help you find the right le_pen. Ready? 😃', quick_replies=quick_reps)
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)
    return 'google'

def reply_second_welcome(body_dict, fb_user_dict, recipient_id, domain_url):
    recipient = messages.Recipient(recipient_id=recipient_id)

    text = '{}! You\'re back ☀️ 🏖️ 🕶️'.format(fb_user_dict['first_name'])
    message = messages.Message(text=text)
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)
    attachment = attachments.ImageAttachment(
            # url=get_static_address(domain_url, 'giphy.gif')
            url='https://media.giphy.com/media/qutisuYBaGfFC/giphy.gif'
    )
    message = messages.Message(attachment=attachment)
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)

    quick_yes = quick_replies.QuickReplyItem(
            content_type='text',
            title='yes 👍🏼',
            payload='YES',
            )
    quick_no = quick_replies.QuickReplyItem(
            content_type='text',
            title='no 👎🏼',
            payload='NO',
            )
    quick_reps = quick_replies.QuickReplies(
            quick_replies= [quick_yes, quick_no],
            )
    message = messages.Message(text='I\'m here to help you find the right le_pen. Ready? 😃', quick_replies=quick_reps)
    request = messages.MessageRequest(recipient, message)
    messenger.send(request)
    return 'google'


def location(body_dict, domain_url):

    latlng = body_dict['originalRequest']['data']['postback']['data']
    # res = gmaps.reverse_geocode(
    #                     latlng=(latlng['lat'], latlng['long']),
    #                     result_type='street_address',
    #                     location_type='ROOFTOP')
    res = gmaps.places(
                    query='sephora',
                    location=(latlng['lat'], latlng['long']),
                    radius=3000,
                    type='store',
                    )
    place_id = res['results'][0]['place_id']

    res = gmaps.place(
                    place_id=place_id,
                    language='en',

                    )

    map_url = json.dumps(res['result']['url'])

    return reply_store_location(body_dict, map_url, domain_url)

def welcome(body_dict, domain_url):
    recipient_id = get_recipient_id(body_dict)
    url = 'https://graph.facebook.com/v2.6/{}'.format(recipient_id)
    params = (
    ('fields', 'first_name,last_name,profile_pic,locale,timezone,gender'),
    ('access_token', settings.MESSENGER_PLATFORM_ACCESS_TOKEN),
    )

    res = requests.get(url, params=params)
    fb_user_dict = json.loads(res._content)
    print fb_user_dict['first_name']
    obj, created = Subscription.objects.get_or_create(
        facebook_sender_id=recipient_id,
        facebook_first_name=fb_user_dict['first_name'],
        facebook_last_name=fb_user_dict['last_name'],
        gender=fb_user_dict['gender'],
    )

    if created:
        return reply_welcome(body_dict=body_dict, fb_user_dict=fb_user_dict,
                            recipient_id=recipient_id, domain_url=domain_url)
    else:
        return reply_second_welcome(body_dict=body_dict, fb_user_dict=fb_user_dict,
                            recipient_id=recipient_id, domain_url=domain_url)




dispatcher = {'location': location,}
