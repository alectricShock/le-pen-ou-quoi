# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Photo, Contender, Voter


# Register your models here.
@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('slug','has_photo')
    # fields = ('slug','image', 'get_contender')
    # readonly_fields = ('date_created', 'date_updated')
    def has_photo(self, obj):
        return obj.image



@admin.register(Contender)
class ContenderAdmin(admin.ModelAdmin):
    list_display = ('slug','votes',)
    list_editable = ('votes',)
    filter_horizontal = ('images',)





@admin.register(Voter)
class VoterAdmin(admin.ModelAdmin):
    list_display = ('created_at',)
    filter_horizontal = ('contenders',)
