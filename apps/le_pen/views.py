# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404
from django.db.models import Sum
from django.conf import settings
from django.http import Http404, HttpResponse
from django.views.generic import View, TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .models import Photo, Contender, Voter
import json
import random


# Create your views here.

class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)

        if not self.request.session.session_key:
            self.request.session.save()

        session_key = self.request.session.session_key
        voter, created = Voter.objects.get_or_create(session_key=session_key)
        # context['tag_line_1'] = 'Vote pour ton candidat favoris'
        if voter.done_voting:
            context['can_vote'] = False
            return context
        else:
            context['can_vote'] = True
            context = self.get_contender(context, voter)
            return context


    def get_contender(self, context, voter):
        contenders = Contender.objects.filter(is_le_pen=False)
        voted_contenders = voter.contenders.all()

        contenders_remaining = [x for x in contenders if x not in voted_contenders]
        if contenders_remaining:
            context['can_vote'] = True
            contender = random.choice(contenders_remaining)
            context['first_contender'] = contender

            return context
        else :
            context['can_vote'] = False


            return context



class StandingsView(TemplateView):
    template_name = "standings.html"

    def get_context_data(self, **kwargs):
        context = super(StandingsView, self).get_context_data(**kwargs)
        contender_list = Contender.objects.filter(is_le_pen=False)
        total_votes = contender_list.aggregate(Sum('votes'))
        context['total_votes'] = total_votes['votes__sum']
        context['total_voters'] = Voter.objects.all().count()
        sorted_contender_list = contender_list.order_by('votes').reverse()
        context['sorted_contender_list'] = sorted_contender_list
        context['tag_line_1_a'] = 'Nous avons recueilli '
        context['tag_line_1_b'] = ' votes pour à peu près n’importe quoi plutôt que Le Pen.'
        context['tag_line_2'] = 'Parce que c’est pas si compliqué de voter contre elle, on compte sur vous dimanche !'

        return context

# selfie for cupon codes cell phone baby bot bun bun demo
class VoteView(View):

    def post(self, request, vote, contender):
        session_key = self.request.session.session_key
        contender_obj = get_object_or_404(Contender, slug=contender)
        voter = Voter.objects.get(session_key=session_key)
        if contender_obj not in voter.contenders.all():
            print 'ininininin'
            voter.contenders.add(Contender.objects.get(slug=contender))
            voter.save()
            self.vote(vote=vote)
            return HttpResponse('voted',)
        else:
            return HttpResponse('already voted')

    def vote(self, vote):
        contender = Contender.objects.get(slug=vote)
        contender.votes +=1
        return contender.save()


class ContenderView(TemplateView):
    template_name = "demo.html"

    def get_context_data(self, slug, **kwargs):
        context = super(ContenderView, self).get_context_data(**kwargs)
        if not self.request.session.session_key:
            self.request.session.save()

        session_key = self.request.session.session_key
        voter, created = Voter.objects.get_or_create(session_key=session_key)

        contender = slug
        context = self.get_le_pen(context)
        context = self.get_contender(context, slug)
        context = self.get_next_contender(context, voter, slug)
        context['tag_line_1'] = 'Vote pour ton candidat favori'
        context['fb_url'] = 'www.lepenroulette.com{}'.format(context['contender'].get_absolute_url())

        return context


    def get_le_pen(self, context):
        le_pen_contender = Contender.objects.get(is_le_pen=True)
        le_pen_img = le_pen_contender.images.all().order_by('?')[0]
        context['le_pen_contender'] = le_pen_contender
        context['le_pen_img'] = le_pen_img
        return context

    def get_next_contender(self, context, voter, slug):
        contenders = Contender.objects.filter(is_le_pen=False)
        voted_contenders = voter.contenders.all()

        contenders_remaining = [x for x in contenders.exclude(slug=slug) if x not in voted_contenders]
        # and not voter.done_voting
        if contenders_remaining :
            context['can_vote'] = True
            contender = random.choice(contenders_remaining)
            context['next_contender'] = contender

            return context
        else :
            contenders_remaining = [x for x in contenders if x not in voted_contenders]
            if contenders_remaining :
                context['last_vote'] = True
                contender = random.choice(contenders_remaining)
                context['next_contender'] = contender
                context['can_vote'] = True

                return context

            else:
                context['can_vote'] = False

                return context

    def get_contender(self, context, slug):
        contender = get_object_or_404(Contender, slug=slug)
        contender_img = contender.images.all().order_by('?')[0]
        context['contender'] = contender
        context['contender_img'] = contender_img
        return context
