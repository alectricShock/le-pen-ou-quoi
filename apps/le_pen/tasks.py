# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
import json
from .utils import (get_recipient_id, get_static_address,
                    location, reply_store_location, welcome)



@shared_task
def call_action_task(body_dict, domain_url):

    action = body_dict['result']['action']
    try:
        return dispatcher[action](body_dict, domain_url)
    except Exception as e:
        raise




dispatcher = {'location': location,'welcome': welcome}
